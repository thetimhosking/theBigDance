# Domain Model
## Overview

The domain model capture essential information about the most important concepts in the system.

## Domains

The foillowing major domains are identified:
1. Person - an individual who has some involvement in sport
2. Organisation - a company, association, or some other sporting body that is made up of a group of people. The organisation has some involvement or interest in sport
3. Person Role - the types of involvement an individual person may have in sport
4. Organisation Role - the types of involement that a sporting body may have
5. Sporting Event - games (competitive or scratch), training or rehab sessions, or other events that involve people or teams, a location or facility, and that are scheduled for a given time. That is, the who, when and where.
6. Competition - organised games involving teams or individuals and where results are kept to determine a winner
7. Scoring and game statistics - the results kept in the peculiar form for a sport
8. Facility - the place, sporting field, court, pitch, or other location at which a game may be played

~~~ mermaid
graph TD;
O--> |manages| F[Facility];
O[Organisation]--> |runs| C[Sports Competition];
P[Person]--> |has a role at|O;
P--> |compete as| PL[Player];
PL--> |is a type of| CR;
P--> |has a role in| T[Team];
T --> |is a type of| CR;
P--> |member of| SC;
CR[Competitor] --> |participates in| E;
C--> |includes| E[Sporting Event];
T--> |represents| SC[Club];
E--> |is held at| F;

~~~


### Person
The domain where the profile of a person is built, including:
- Identity information (name, age, sex, etc.)
- Contact details

### Person Role
All the types of involvement a person may have in sport:
- Player: someone who participates in games and other sporting events
- Supporter (or follower): a person who attends or watches games involving a favourite player or team, and may identify with that team or person
- Coach: a person who assists individuals or teams to improve their skills
- Official: an umpire, referee, judge, scorer, timekeeper or other capacity in which a person assists at the level of the game
- Team manager: a person responsible for keeping a team's profile up to date and for adding/removing team members
- Player manager: provides services to players 
- Sports administrator
- Personal trainer
- Sports medicine professional
- Organisation contact: a person at a club, sporting body, or 


### Organisation Role
All the types of involvement an organisation may have in sport:
- Sports club: an organisation represented by individuals and teams competing 
- Sporting body: organises and manages competitions
- Sponsor: an organisation that pays a player or team for their representation, or provides other financial assistance, e.g. provides uniforms or equipment
- Sports Management: an organisation that provides commercial services to a player, such as, organising deals with sponsors

### Sporting event / fixture
A sporting event can be one of:
- Competitive game: an actual competitive event that invovles competitors (players or teams). Depending on the sport it may be referred to as a round, match, tie, leg, rubber, heat, bout, fixture, contest, etc.
- Scratch game: a less formal competitive event involving players or teams and is held at a given location and time (as for a competitive game). A score may be kept. However, a scratch game does not contribute to results in a competition
- Training session: held at a given time, and location or facility, involving players and teams. There may be a plan of activities. Results may be recorded, such as times, distances, etc.
- Rehab session: allowing for the recording of an athlete's recovery from injury

### Scoring and game statistics
Each sport has its own peculiar way of recording results and the actions of players and teams during a game. For example, tennis has it's own method of scoring that is peculiar to other sports. In addition, statistics during a game are recorded, such as, number of aces, number of first serves, number of winners hit, number of unforced errors, etc.

### Competition
An organised number of events that involves a schedule and that results in a winning team or player. Depending on the sport, it may be referred to as a tournament, championship, meet, etc.

A competition may include a series of games for finals, or may be made up of "round robin" events and then knock-out games. 

Competitions may be broken into seasons, grades, etc. For example, a tennis competition may be run every year (season) and may have an A, B, and C grades. 

Competitions may also be based on sex, i.e. men's women's, mixed, etc. It is assumed that each would result in a different competition, e.g. AFL and AFLW, NBL and WNBL.

A competition, therefore, may be made up of:
- Season 
- Grade
- Round
- Finals series

