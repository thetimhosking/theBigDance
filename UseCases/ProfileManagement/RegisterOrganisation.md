# Register a sporting organisation

## Overview

A person registers an organisation, creating a profile for its involvement in sports. , links to clubs and approvals for being approached by other persons and organisations.

# User Role
**Individual**

# Data
**1. Organisation details:**
- Name of organisation
- Description of the organisation
- Location (used to identify potential players, clubs, teams, competitions, etc.)
- Profile picture

**2. Web URLs:**
- List of URLS
    - Website URL
    - LinkedIn URL
    - Twitter URL
    - Facebook URL
    - Instagram URL


**3. Contact details:**
- Email
- Phone


**4. Sporting interests:**
- Text statement describing sporting interests

**5. Sport roles:**
List of:
- Sport type (e.g. tennis, golf, football, cricket, rugby, etc.)
- Role in the sport (competition owner, sports management, player management, etc.)


**6. Approvals for interaction**
- Can be invited to sponsor players, teams, clubs
- Can be approached by service providers

## Normal flow
1. Individual logs in
1. Individual provides organisation profile, URL, contact, and other organisation data
1. System validates data
1. System records organisation profile and makes profile visible as per approvals

## Exception flow
System determines the account is not genuine and returns error
