# Overview
A person registers a team of players to train and compete together.

# User Role
**Registered Player**

**Team administrator**

This use case results in the registered player being granted privileges as the team admninistrator. 

# Data
**1. Person profile**

Identity of the registered player to act as team administrator

**2. Team details**
- Team name (New York Jets)
- Team description (text statement)
- Team colours (Green and white)
- Team nickname/mascot (Jets)

**3. Sports**

List of:
- Sport type (e.g. indoor soccer, netball, tennis doubles, etc.) that the team is interested in. This list assists competition owners to invite teams who may be interested in registering.


**4. Approvals**
- Can be invited to join formal competitions 
- Can be invited to informal games by other teams
- Can be approached by players wanting to join
- Can be approached by sponsors
- Can be approached by coaches
- Can be approached by fitness trainers
- Can be approached by sports medicine professionals
- Can be approached by clubs about membership or other services

**5. Players**

List of:
- Nominated players to join team


# Normal flow
1. Registered player logs in
1. Registered player selects command to create a team
1. Registered player provides team details
1. System validates data
1. System creates team and saves the record
1. System creates homepage for team
1. System gives registered player team administrator rights
1. System redirects registered player to team home page
1. Team administrator Registered player optionally selects command to invite players to join team
1. Team administrator searches for, and selects, nominated players to join the team
1. The system sends invitations to the invited players, both as notifications in the app and as email
1. The invited players receive the invitation and optionally accept 
1. When the system receives an acceptance to an invitation it makes the player a member of the team
1. The system notifies the team administrator that the player has accepted the invitation

# Exception flow
1. System determines the team data is invalid and returns an error
1. The system determines the invitation is not valid and terminates the process