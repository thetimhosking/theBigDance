# Accept Team Invitation

## Overview
A person receives an invitation to join a team and accepts or rejects the invitation. 

## User Role
**Individual player**

## Data
**1. Team invitation:**
- Team administrator identity
- Team identity
- Team web page
- Invitation message


## Normal flow
1. The individual player receives an invitation to join a team via:
    1. Email
        - The individual clicks on a link in the email to accept
        - The individual may ignore the invitation
    1. Notification in the individual player's homepage
        - The individual selects the command to accept the invitation
        - The individual may select the command to reject the invitation
1. If the individual player selects the command to accept the invitation
    - System registers the individual as a member of the team
    - System notifies the team administrator that the individual has been added to the team

## Exception flow

