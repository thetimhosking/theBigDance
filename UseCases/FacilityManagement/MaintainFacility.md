# Overview

A person acting as representative for a sporting organisation registers a facility where sporting events can be held. For example, an 

# User Role
**Individual**

# Data
**1. Person Identity details:**
- URL of person registering facility

**2. Facility owner details:**
- Organisation URL (the URL of the orgnisation that owns or operates the facility)

**3. Address details:**
- Street address

**4. Facility details:**
- Name of facility
- Description of facility
- List of:
    - Courts, pitches, grounds, etc. within the faclity
    - Types of sports that can be played on the above [may be a later feature]
- Description of the type of equipment available at the facility
 

**4. Contact details:**
- Email
- Phone
- Links to other social media accounts if the facility has its own accounts
- Preferred method of contact (i.e. email, text message, social media message)


# Normal flow
1. Person creates account login for the facility, linked to an organisation as a manager of a sporting facility
1. Person provides data for the facility
1. System validates data
1. System uses the physical address to locate the geo coordinates for the facility
1. System records the facility profile and makes profile visible for bookings of sporting events

# Exception flow
System determines the account is not genuine and returns error
