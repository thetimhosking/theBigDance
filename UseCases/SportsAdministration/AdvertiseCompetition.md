# Advertise Competition

## Overview
A sporting competition is advertised to potential players and teams to register.

## User role
Sports administrator

## Data

**Competition details**
Competition name
Sport type - the sport to be played in the competition, including an indication as to whether the sport is a team or individual sport
Start date 
Description - a text statement describing the type of competition

**Location**
The addresses and sports facilities where the competition will be played

**Season**
Season detals - e.g. 2020 summer season

**Grade**
Age range - e.g. juniors, seniors, under 12s, under 19, over 50s, open, etc.
Gender - male, female, mixed, etc.

**Reigstration details**
Fee structure - cost per individual / team
Link (URL) to website for registration
A later release will provide online registration and payment

## Normal flow
1. The sports administrator selects the command to advertise a competition
1. The sports administrator enters the data required
1. The system validates the data and raises errors where necessary
1. The system creates an entry on the sports administrator's hompeage for the competition with a link to the sports administrator's website


## Exception flow
1. The validation of the data supplied raises an exception and the user is prompted to correct the errors
