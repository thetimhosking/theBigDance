# Create Team Game

## Overview
A team administrator invites another team(s) to a game, or records that a game was played. Some types of games (sporting events) involve more than one team at a time, e.g. an ambrose golf round, cycling, etc.

## User role
Team administrator

## Data
1. Identity of team administrator

1. What - the type of game

1. When - the date and time the game is to be / was played

1. Where - the location of the sporting facility where the game is played

1. Who - the teams involved


## Normal flow
1. The team administrator selects the command to create a team game from the team's homepage

1. The team administrator selects the type of game from the list of sports the team plays

1. The team administrator enters the date and time of the game

1. The team administrator enters the location of the game
    1. The team administrator searches for the location by entering a part of the address or by supplying the name of sporting facility (e,g, the name of an indoor cricket facility known by the system)
    1. The system validates the address
    1. In the case of a sporting complex with multiple facilities, the team administrator can optionally enter the name of the specific facility, e.g. pitch 3

1. The team administrator enters the names of the teams involved:
    1. If the game is in the future:
        1. Teams can be invited to the game by the team administrator selecting the teams from those in the network of the team
        1. The system sends invitations to the teams' administrators via email and by posting notifications to the team's homepage and the homepages of each of the administrators
        1. A team administrator chooses to accept the invitation
        1. If the invitation is accepted, the system creates a record in the list of upcoming games for the invited team
        1. Once the game is played (it is in the past) the scores and result of the game can be entered by the team administrator
        1. The system creates an entry in the game history of the team, with hyperlinks to a page to the game and to each team involved        

    1. If the game has already been played:
        1. The team administrator can simply select the teams involved from those in the network. 
        1. The system notifies the team that the game is being recorded. The team administrator of that team can object to the team being involved in the game being recorded. However, the game can still be recorded
        1. The scores and result of the game is entered by the team administrator
        1. The system creates an entry in the game history of the team, with hyperlinks to a page to the game and to each team involved

## Exception Flow
1. For a game scheduled for the future, an invited team rejects the invitation
    1. The invited team is removed from the list of teams involved

1. For a game played in the past where no invitations are sent, the involved team objects to the recording of a result
    1. The involved team is only recorded as a text entry and no hyperlink is created to the team's homepage



