# Create individual game

## Overview
A registered player records a game, which may involve just him/herself or multiple players. The game may have occurred in the past or it may be scheduled for the future. The players, date and time, and location are recorded. 

If the game is part of a competition owned by an organisation the individual cannot record the result - the result must be entered by the competition owner.

## User role
**Individual**

## Data

1. Individual identity

1. What - the type of game or sport to be (or has been) played

1. Who - Players involved

1. When - Date and time

1. Where - the location of the game
    - Street address, e.g. 12 Elm Street, Parksville
    - sports facility at the location (optional) e.g. Court 1

1. Results - the scores (optional)
    - Score for each player using a template for the sport
    - Only allowed if the game is not part of an owned competition

1. Game statistics (optional)
    - In addition to the final score, data about the actions of each player using a template for the sport
    - For example, the template for a game of golf would include a scroe for each hole

## Normal flow 
1. Individual selects the command to record/schedule a game
    1. The system takes the individual to a homepage for the game
1. The individual selects the **type of game** to be played from the list of sports nominated in his/her profile
1. The individual enters the **date and time** of the game, which may be in the past
1. The individual enters the **location** of the game, using a street address
    1. The individual provides a description or name of the location
    1. The individual optionally selects the command to look up an address for a sporting facility (e.g. a golf course)
        1. The system searches for the location based on the address or name of the facility and presents results to the individual
        1. The individual selects the location found
        1. The individual optionally selects from a list of facilities at the address (e,g. court 1, or front 9)
1. The individual can optionally select to save the game before going on and, if so, the system keeps the indivdual on the game homepage
1. The individual optionally selects the command to **invite players**. Note that other players do not need to be involved: the individual may just be recording the result of a game, e.g. their own score in a golf game
    
    1. The system provides a list of players connected to the individual to invite to the game
    
    1. The individual selects players to invite to the game, or provides email addresses of people to invite to the game
    
    1. The system sends invitations to the selected players, through a notification in the system and via email. If only an email is provided for the invitation because the invited player does not yet have a profile, the system reserves a profile for the invited player and posts the invitation as a notification in the reserved profile

    1. The invited players accept the invitation, either through a notification on their own hompeage or through a link in the invitation email. 
        1. If the invitation is only sent via email, the invited player is presented with the option to also create their profile. If the option to create the profile is accepted, then the system also takes the invited player to their profile page to complete their information.

    1. The system informs the individual of responses received

1. If the game is in the past, the indivdual records the **score**.

1. The system offers the option to record the result and each player's statistics from the game. [This may be a later release]



