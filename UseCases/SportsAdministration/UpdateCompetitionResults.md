# Update Competition Results
The owner of a competition, either an individual or a sports administration organsiation, records the results of one or more games. Typically this is the results from a round of fixtures. However, results may be entered on a game by game basis.


## User role
**Individual**
**Sports administrator**

## Data

1. Identity of individual with authority to record resutls of a competition

1. What - the results for each game or event
    - the identifier for each game or event
    - the ystem will know the players or teams invovled in each event
    - the results for each player or team involved in the game or event
    - a template for each type of sport or event is used to record the results
    - the time period at which the score applies, allowing for progressive scores (a later release)
    - in indicator as to whether the score is in progress or final


1. Game statistics (optional)
    - In addition to the final score, data about the actions of each player using a template for the sport
    - For example, the template for a game of golf would include a score for each hole

## Normal flow 
1. Sports administrator selects the command to record the results of a game or event
    1. The system takes the individual to a homepage for the game

1. The sports admininstrator selects the command to record the result of the game
    1. The result is entered according to a template for the type of sport or event, e.g. time achieved, points scored, etc.
    1. The system determines the overall winner, and any place winners, based on the sport and results entered 
    1. The system awards competition points according to the parameters of the competition
    1. The sports administrator confirms the points and any bonus points or penalties that need to be applied

1. The system offers the option to record player's statistics from the game. [This may be a later release]



