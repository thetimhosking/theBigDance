-- Create and use 
IF db_id(N'Sportsv01') is not NULL
BEGIN
  use Sportsv01
END
ELSE
BEGIN
  CREATE DATABASE Sportsv01
  USE Sportsv01
END
GO

-- Create schema for testing
DECLARE @schema_name nvarchar(200), @sql nvarchar(200)
SET @schema_name = 'ante' 
SET @sql = 'CREATE SCHEMA ' + @schema_name

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = @schema_name )
BEGIN
EXEC sp_executesql @sql
PRINT @sql + ' completed'
END
GO

-- Create schema for tables and views
DECLARE @schema_name nvarchar(200), @sql nvarchar(200)
SET @schema_name = 'gnosis'
SET @sql = 'CREATE SCHEMA ' + @schema_name

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = @schema_name )
BEGIN
EXEC sp_executesql @sql
PRINT @sql + ' completed'
END
GO

-- Create schema for stored procedures
DECLARE @schema_name nvarchar(200), @sql nvarchar(200)
SET @schema_name = 'diakonos' -- servant
SET @sql = 'CREATE SCHEMA ' + @schema_name

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = @schema_name )
BEGIN
EXEC sp_executesql @sql
PRINT @sql + ' completed'
END
GO

-- Create schema for freeServ
DECLARE @schema_name nvarchar(200), @sql nvarchar(200)
SET @schema_name = 'freeServ' -- administrator
SET @sql = 'CREATE SCHEMA ' + @schema_name

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = @schema_name )
BEGIN
EXEC sp_executesql @sql
PRINT @sql + ' completed'
END
GO


-- Create schema for premServ
DECLARE @schema_name nvarchar(200), @sql nvarchar(200)
SET @schema_name = 'premServ' -- administrator
SET @sql = 'CREATE SCHEMA ' + @schema_name

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = @schema_name )
BEGIN
EXEC sp_executesql @sql
PRINT @sql + ' completed'
END
GO


-- Create schema for admin
DECLARE @schema_name nvarchar(200), @sql nvarchar(200)
SET @schema_name = 'oikonomon' -- administrator
SET @sql = 'CREATE SCHEMA ' + @schema_name

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = @schema_name )
BEGIN
EXEC sp_executesql @sql
PRINT @sql + ' completed'
END
GO




GO