set @birth_date = convert( date, '15 April 2000')
set @birth_locality = "Punchbowl"
set @birth_country = "Australia"
set @birth_state = "NSW"

exec @return_val = dbo.PersonInsert 
exec @return_val = dbo.PersonNameInsert
exec @return_val = dbo.PersonSportRoleInsert