IF OBJECT_ID(N'dbo.CreatePersonProfile', N'P')IS NOT NULL  
   DROP PROCEDURE dbo.CreatePersonProfile;  
GO  
create procedure CreatePersonProfile(
	@BirthDate date,
	@BirthLocalityName varchar(80),
	@BirthCountryName varchar(80),
	@ActiveIndicator bit,
	@GenderCode char(1),
	@FamilyName varchar(80),

)
as 
declare @return_value smallint

set nocount on

insert Person(
	BirthDate,
	BirthLocalityName,
	BirthCountryName,
	ActiveIndicator,
	GenderCode
	)
values(
	@BirthDate,
	@BirthLocalityName,
	@BirthCountryName,
	@ActiveIndicator,
	@GenderCode
	)


-- Return values
select @return_value = @@ROWCOUNT

RETURN @return_value

go


