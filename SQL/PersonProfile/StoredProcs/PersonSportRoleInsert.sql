IF OBJECT_ID(N'dbo.PersonSportRoleInsert', N'P')IS NOT NULL  
   DROP PROCEDURE dbo.PersonSportRoleInsert;  
GO  
create procedure PersonSportRoleInsert
(
	@PersonID bigint,
	@SportName varchar(80),
	@RoleType varchar(80),
	@BeginDate date
)

as 
declare @return_value smallint

set nocount on

insert PersonSportRole
(
	PersonID,
	SportName,
	RoleType,
	BeginDate
)
values
(
	@PersonID,
	@SportName,
	@RoleType,
	@BeginDate
)


-- Return values
select @return_value = @@ROWCOUNT

RETURN @return_value 

go


