IF OBJECT_ID(N'dbo.PersonNameInsert', N'P')IS NOT NULL  
   DROP PROCEDURE dbo.PersonNameInsert;  
GO  
create procedure PersonNameInsert
(
	@PersonID bigint,
	@FamilyName varchar(80),
	@FirstName varchar(80),
	@MiddleName varchar(80),
	@BeginDate date
)

as 
declare @return_value smallint

set nocount on

insert PersonName
(
	PersonID,
	FamilyName,
	FirstName,
	MiddleName,
	BeginDate
)
values
(
	@PersonID,
	@FamilyName,
	@FirstName,
	@MiddleName,
	@BeginDate
)


-- Return values
select @return_value = @@ROWCOUNT

RETURN @return_value

go


