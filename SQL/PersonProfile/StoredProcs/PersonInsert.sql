IF OBJECT_ID(N'dbo.PersonInsert', N'P')IS NOT NULL  
   DROP PROCEDURE dbo.PersonInsert;  
GO  
create procedure PersonInsert(
	@UserName nvarchar(255),
	@FamilyName nvarchar(255), 
	@FirstName nvarchar(255), 
	@MiddleName nvarchar(255),
	@LastName nvarchar(255),
	@NickName nvarchar(255),
	@ProfileText nvarchar(4096),
	@Email nvarchar(512),
	@BirthDate date,
	@BirthYear char(4),
	@BirthCountryName nvarchar(255),
	@BirthStateName nvarchar(255),
	@BirthLocalityName nvarchar(255),
	@GenderCode char(1)

)
as 
declare 
	@return_value smallint,
	@PersonID bigint


set nocount on

insert Person(
	UserName,
	FirstName,
	MiddleName,
	LastName,
	NickName,
	ProfileText,
	Email,
	BirthDate,
	BirthLocalityName,
	BirthStateName,
	BirthCountryName,
	GenderCode
	)
values(
	@UserName,
	@FirstName,
	@MiddleName,
	@LastName,
	@NickName,
	@ProfileText,
	@Email,
	@BirthDate,
	@BirthLocalityName,
	@BirthStateName,
	@BirthCountryName,
	@GenderCode
	)


set @PersonID = SCOPE_IDENTITY()

-- Set error values


-- Return values
select @return_value = @PersonID

RETURN @return_value

go


