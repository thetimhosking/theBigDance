IF OBJECT_ID(N'oikonomon.sport_role', N'U')IS NOT NULL  
   DROP TABLE oikonomon.sport_role;  
GO
CREATE TABLE oikonomon.sport_role( 
	id int IDENTITY(1,1) PRIMARY KEY,
	row_guid UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID() ROWGUIDCOL,
	name NVARCHAR(120),
	sequence_number INT,
	row_id rowversion
)


IF OBJECT_ID(N'oikonomon.sport_skill', N'U')IS NOT NULL  
   DROP TABLE oikonomon.sport_skill;  
GO
CREATE TABLE oikonomon.sport_skill( 
	id int IDENTITY(1,1) PRIMARY KEY,
	row_guid UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID() ROWGUIDCOL,
	name NVARCHAR(120),
	sequence_number INT,
	row_id rowversion
)


IF OBJECT_ID(N'oikonomon.team_role', N'U')IS NOT NULL  
   DROP TABLE oikonomon.team_role;  
GO
CREATE TABLE oikonomon.team_role( 
	id int IDENTITY(1,1) PRIMARY KEY,
	row_guid UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID() ROWGUIDCOL,
	name nvarchar(120),
	sequence_number INT,
	row_id rowversion
)



IF OBJECT_ID(N'oikonomon.team_position', N'U')IS NOT NULL  
   DROP TABLE oikonomon.team_position;  
GO
CREATE TABLE oikonomon.team_position( 
	id int IDENTITY(1,1) PRIMARY KEY,
	row_guid UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID() ROWGUIDCOL,
	name NVARCHAR(120),
	team_role_id INT,
	sequence_number INT,
	row_id ROWVERSION
)


ALTER TABLE oikonomon.team_position
   ADD CONSTRAINT FK_team_position_team_role_id FOREIGN KEY (team_role_id)
      REFERENCES oikonomon.team_role (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE

