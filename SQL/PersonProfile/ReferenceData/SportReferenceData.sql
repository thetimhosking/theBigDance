
IF OBJECT_ID(N'oikonomon.sport_category', N'U')IS NOT NULL  
   DROP TABLE oikonomon.sport_category;  
GO
CREATE TABLE oikonomon.sport_category( 
	id int IDENTITY(1,1) PRIMARY KEY,
	row_guid UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID() ROWGUIDCOL,
	name NVARCHAR(120),
	sequence_number INT,
	row_id ROWVERSION
)



IF OBJECT_ID(N'oikonomon.sport', N'U')IS NOT NULL  
   DROP TABLE oikonomon.sport;  
GO

CREATE TABLE oikonomon.sport( 
	id int IDENTITY(1,1) PRIMARY KEY,
	row_guid UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID() ROWGUIDCOL,
	name NVARCHAR(120),
	sport_category_id INT,
	sport_sub_category INT,
	sequence_number INT,
	row_id ROWVERSION
)


ALTER TABLE oikonomon.sport_type
   ADD CONSTRAINT FK_sport_type_sport_sub_category_id FOREIGN KEY (sport_sub_category_id)
      REFERENCES oiknonomon.sport_category (id)
      ON DELETE CASCADE					
      ON UPDATE CASCADE

