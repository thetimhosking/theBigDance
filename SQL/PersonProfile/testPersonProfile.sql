use BigDance01
go

select top 100 ID, UserName, LastName, FirstName from Person

-- Test add a person profile
declare @person_id smallint
declare @test_result smallint
declare @birth_date date
declare @birth_country varchar(80)
declare @birth_locality varchar(80)
declare @birth_state varchar(80)

set @birth_date = convert( date, '15 April 2000')
set @birth_locality = 'Punchbowl'
set @birth_country = 'Australia'
set @birth_state = 'NSW'

declare 
	@PersonID bigint,
	@UserName varchar(80),
	@FamilyName varchar(80),
	@FirstName varchar(80),
	@MiddleName varchar(80),
	@BeginDate date,
	@NickName varchar(80),
	@Email varchar(300),
	@ProfileText varchar(2048)

set @UserName = 'TheBigTom'
set @FamilyName = 'Jones'
set @FirstName = 'Tom'
set @MiddleName = 'Q'
set @BeginDate = GETDATE()
set @NickName = 'Big Tom'
set @Email = 'BigTom@myemail.com'
set @ProfileText = 'the best footballer ever'


declare @Sport1 varchar(80), @Role1 varchar(80)
declare @Sport2 varchar(80), @Role2 varchar(80)
declare @Sport3 varchar(80), @Role3 varchar(80)


set @Sport1 = 'Tennis'
set @Role1 = 'Player'
set @Sport2 = 'Tennis'
set @Role2 = 'Coach'
set @Sport3 = 'Cricket'
set @Role3 = 'Follower'



/*
declare @PersonData NVARCHAR(MAX);
set @PersonData = N'[
	{"Person": {"BirthDate": "2000-15-16", "BirthLocality": "Punchbowl", "BirthState": "NSW", "BirthCountry": "Australia", "ActiveIndicator": 1, "GenderCode": "M"}
	 "PersonName": {"FamilyName": "Jones", "FirstName": "Tom", "MiddleName": "Q", "BeginDate": "2020-07-31", "EndDate": ""}
	 "Sports":	{"Sport": "Tennis", "Role": "Player"}
				
*/



execute @PersonID = dbo.PersonInsert @UserName, @FirstName, @MiddleName, @FamilyName, @NickName, @ProfileText, @Email, @birth_date, @birth_country, @birth_state, @birth_locality, "M"
--execute @test_Result = dbo.PersonNameInsert @PersonID, @BeginDate
execute @test_Result = dbo.PersonSportRoleInsert @PersonID, @Sport1, @Role1, @BeginDate
execute @test_Result = dbo.PersonSportRoleInsert @PersonID, @Sport2, @Role2, @BeginDate
execute @test_Result = dbo.PersonSportRoleInsert @PersonID, @Sport3, @Role3, @BeginDate


print @test_result




Go

-- Test that proc CreatePersonProfile 
select * from Person
--select * from PersonFormerName
select * from PersonSportRole

go


SELECT 
	p.ID AS 'Person.ID', p.BirthDate AS 'Person.BirthDate', 
	Names.FirstName AS 'PersonName.FirstName', Names.MiddleName AS 'PersonName.MiddleName', Names.FamilyName AS 'PersonName.FamilyName', Names.BeginDate AS 'PersonName.BeginDate',
	Sports.SportName AS 'PersonSport.Name', Sports.RoleType AS 'PersonSport.RoleType'
FROM Person p
JOIN PersonName Names
ON	 p.ID = Names.PersonID
JOIN PersonSportRole Sports
ON   p.ID = Sports.PersonID
FOR JSON AUTO, ROOT('Person');


declare @json nvarchar(max)
set @json =  N'{["ID":1,"BirthDate":"2000-04-15", "FirstName":"Tom", "MiddleName":"Q", "LastName":"Jones", "NickName":"Jonesboy", "ProfileText":"The best damn footballer",
			"Sports":	[{"Name":"Tennis","RoleType":"Player"},
						 {"Name":"Tennis","RoleType":"Coach"},
						 {"Name":"Cricket","RoleType":"Follower"}
						]
				]}';



SELECT *
FROM OPENJSON(@json, N' Person.FirstName')

  

declare @json nvarchar(max)
set @json =  N'[{"ID":1, "BirthDate": "2000-04-15", "FirstName": "Tom", "MiddleName": "Q", "LastName": "Jones", "NickName": "Jonesboy", "ProfileText": "The best damn footballer",
			"Sports": [ {"Name": "Tennis", "RoleType": "Player"},
					    {"Name": "Tennis", "RoleType": "Coach"},
						{"Name": "Cricket", "RoleType": "Follower"}
					
				}]';



SELECT *
FROM OPENJSON(@json)
	WITH (
		id BIGINT '$.ID',
		FirstName varchar(80) '$.FirstName',
		LastName varchar(80) '$.LastName',
		Sports nvarchar(max) '$.Sports' AS JSON
		)
OUTER APPLY OPENJSON(Sports)
	WITH (Sport VARCHAR(25) '$');

SELECT *
FROM OPENJSON(@json)
	WITH (
		ID BIGINT '$.ID',
		SportName varchar(80) '$.Sports.Name',
		SportRole varchar(80) '$.Sports.RoleType'
		)
