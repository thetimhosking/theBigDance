IF OBJECT_ID(N'gnosis.person', N'U')IS NOT NULL  
   DROP TABLE gnosis.person;  
GO
create table gnosis.person( 
	id bigint IDENTITY(1,1) PRIMARY KEY,
	row_guid uniqueidentifier NOT NULL DEFAULT NEWID() ROWGUIDCOL,
	user_name nvarchar(120) NOT NULL,
	profile_text nvarchar(2048),
	last_name nvarchar(120) not null,
	first_name nvarchar(120),
	middle_name nvarchar(120),
	fomrer_name nvarchar(240),
	nick_name nvarchar(120),
	email_address nvarchar(350),
	birth_date date,
	birth_month_number tinyint,
	birth_day_numer tinyint,
	birth_year_number smallint,
	birth_locality_name nvarchar(200),
	birth_state_name varchar(80),
	birth_country_name varchar(80),
	gender_code nchar(3),
	row_id rowversion
)


IF OBJECT_ID(N'gnosis.person_sport_role', N'U')IS NOT NULL  
   DROP TABLE gnosis.person_sport_role;  
GO
create table gnosis.person_sport_role( 
	id bigint IDENTITY(1,1) PRIMARY KEY,
	row_guid uniqueidentifier NOT NULL DEFAULT NEWID() ROWGUIDCOL,
	person_id bigint,
	sport_role_id int,
	sport_id int,
	begin_date date not null,
	end_date date,
	row_id rowversion
)

ALTER TABLE gnosis.person_sport_role
   ADD CONSTRAINT FK_person_sport_role_person_id FOREIGN KEY (person_id)
      REFERENCES gnosis.person (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE

ALTER TABLE gnosis.person_sport_role
   ADD CONSTRAINT FK_person_sport_role_sport_role_id FOREIGN KEY (sport_role_id)
      REFERENCES oiknonomon.sport_role (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE

ALTER TABLE gnosis.person_sport_role
   ADD CONSTRAINT FK_person_sport_role_sport_id FOREIGN KEY (sport_id)
      REFERENCES oiknonomon.sport_type (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE

go


